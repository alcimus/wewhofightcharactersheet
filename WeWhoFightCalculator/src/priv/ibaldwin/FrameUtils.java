package priv.ibaldwin;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class FrameUtils {
	
	public static JLabel makeLabel(String label, int xLocation, int yLocation, int width, int height) {
		JLabel newLabel = new JLabel(label);
		newLabel.setBounds(xLocation, yLocation, width, height);
		return newLabel;
	}
	
	public static JCheckBox makeCheckbox(String label, boolean selected, int xLocation, int yLocation) {
		return makeCheckbox(label, selected, xLocation, yLocation, 200, 23, null);
	}
	
	public static JCheckBox makeCheckbox(String label, boolean selected, int xLocation, int yLocation, ActionListener al) {
		return makeCheckbox(label, selected, xLocation, yLocation, 200, 23, al);
	}
	
	public static JCheckBox makeCheckbox(String label, boolean selected, int xLocation, int yLocation, int width, int height, ActionListener al) {
		JCheckBox checkBox = new JCheckBox(label);
		checkBox.setBounds(xLocation, yLocation, width, height);
		checkBox.setEnabled(false);
		checkBox.setSelected(selected);
		if(al != null) 
			checkBox.addActionListener(al);
		return checkBox;
	}
	
	public static JTextField makeDisabledTextField(int xLocation, int yLocation) {
		return makeDisabledTextField(null, xLocation, yLocation);
	}

	public static JTextField makeDisabledTextField(String text, int xLocation, int yLocation) {
		return makeDisabledTextField(text, xLocation, yLocation, 56, 20);
	}

	public static JTextField makeDisabledTextField(String text, int xLocation, int yLocation, int width, int height) {
		return makeTextField(text, false, xLocation, yLocation, width, height, null);
	}
	
	public static JTextField makeEnabledTextField(int xLocation, int yLocation) {
		return makeEnabledTextField(null, xLocation, yLocation, null);
	}
	
	public static JTextField makeEnabledTextField(String text, int xLocation, int yLocation) {
		return makeEnabledTextField(text, xLocation, yLocation, null);
	}
	
	public static JTextField makeEnabledTextField(int xLocation, int yLocation, ActionListener al) {
		return makeEnabledTextField(null, xLocation, yLocation, al);
	}
	
	public static JTextField makeEnabledTextField(String text, int xLocation, int yLocation, ActionListener al) {
		return makeEnabledTextField(text, xLocation, yLocation, 56, 20, al);
	}
	
	public static JTextField makeEnabledTextField(String text, int xLocation, int yLocation, int width, int height) {
		return makeTextField(text, true, xLocation, yLocation, width, height, null);
	}

	public static JTextField makeEnabledTextField(String text, int xLocation, int yLocation, int width, int height, ActionListener al) {
		return makeTextField(text, true, xLocation, yLocation, width, height, al);
	}
	
	
	public static JTextField makeTextField(String text, boolean enabled, int xLocation, int yLocation, int width, int height, ActionListener al) {
		JTextField textField = new JTextField();
		textField.setColumns(10);
		if(text != null)
			textField.setText(text);
		textField.setEnabled(enabled);
		textField.setEditable(enabled);
		textField.setBounds(xLocation, yLocation, width, height);
		if(al != null)
			textField.addActionListener(al);
		return textField;
	}
	
	public static JButton makeEnabledButton(String label, int xLocation, int yLocation, ActionListener al) {
		return makeEnabledButton(label, xLocation, yLocation, 80, 20, al);
	}
	
	public static JButton makeEnabledButton(String label, int xLocation, int yLocation, int width, int height, ActionListener al) {
		return makeButton(label, true, xLocation, yLocation, width, height, al);
	}
	
	public static JButton makeDisabledButton(String label, int xLocation, int yLocation) {
		return makeDisabledButton(label, xLocation, yLocation, 80, 20, null);
	}

	public static JButton makeDisabledButton(String label, int xLocation, int yLocation, ActionListener al) {
		return makeDisabledButton(label, xLocation, yLocation, 80, 20, al);
	}

	public static JButton makeDisabledButton(String label, int xLocation, int yLocation, int width, int height) {
		return makeButton(label, false, xLocation, yLocation, width, height);
	}

	public static JButton makeDisabledButton(String label, int xLocation, int yLocation, int width, int height, ActionListener al) {
		return makeButton(label, false, xLocation, yLocation, width, height, al);
	}
	
	public static JButton makeButton(String label, boolean enabled, int xLocation, int yLocation) {
		return makeButton(label, enabled, xLocation, yLocation, 80, 20);
	}
	
	public static JButton makeButton(String label, boolean enabled, int xLocation, int yLocation, int width, int height) {
		return makeButton(label, enabled, xLocation, yLocation, width, height, null);
	}
	
	public static JButton makeButton(String label, boolean enabled, int xLocation, int yLocation, int width, int height, ActionListener al) {
		JButton button = new JButton(label);
		button.setBounds(xLocation, yLocation, width, height);
		button.setEnabled(enabled);
		if(al != null)
			button.addActionListener(al);
		return button;
	}
	
}
