package priv.ibaldwin;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import priv.ibaldwin.Statuses.Status;
import priv.ibaldwin.abilities.Ability;

public class StatusFrame {
	private class StatusUIElements{
		JLabel name;
		JTextField curDuration;
		JTextField maxDuration;
		JButton onHitButton;
		JButton useAbilityButton;
		JButton resetAbilityButton;
	}
	
	private JPanel statusPane;
	private PlayerCharacter character;
	private HashMap<String, StatusUIElements> boonUiMap;
	private MainFrame root;
	
	public StatusFrame(MainFrame root, PlayerCharacter character){
		this.root = root;
		this.character = character;
		this.boonUiMap = new HashMap<String, StatusUIElements>();
		statusPane = new JPanel();
		statusPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		statusPane.setLayout(null);
		
		statusPane.add(FrameUtils.makeEnabledButton("Add Status", 10, 10, 160, 20, new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
                JDialog dialog = new JDialog(root, "Modal Dialog", true);
                dialog.setSize(600, 400);
                dialog.setLayout(null);
                dialog.setResizable(false);

                
                JLabel label = FrameUtils.makeLabel("This is a modal dialog", 5, 5, 100, 20);
                dialog.add(label);

                JButton addStatusButton = FrameUtils.makeEnabledButton("Add", 200, 325, new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                    	dialog.dispose();
                    }
                });
                dialog.add(addStatusButton);
                
                
                JButton cancelButton = FrameUtils.makeEnabledButton("Cancel", 300, 325, new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        dialog.dispose(); 
                    }
                });
                dialog.add(cancelButton);

                
                dialog.setVisible(true); // Display the dialog
            }
		}));
		
		for(int i = 0; i < character.getStatuses().size(); i++){
			addStatus(i, character.getStatuses().get(i));
		}
		
	}
	
	private void addStatus(int index, Status cur) {
		int yLocation = index * 30+40;
		int xLocation = 10;
		StatusUIElements uie = new StatusUIElements();
		
		uie.name = FrameUtils.makeLabel(cur.getName(), xLocation, yLocation, 50, 10);
		statusPane.add(uie.name);
		
		uie.curDuration = FrameUtils.makeDisabledTextField(String.valueOf(cur.getDurationRemamining()), xLocation+60, yLocation);
		statusPane.add(uie.curDuration);
		uie.maxDuration = FrameUtils.makeEnabledTextField(String.valueOf(cur.getDurationMax()), xLocation+120, yLocation, new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				cur.setDurationMax( Integer.parseInt(uie.maxDuration.getText()));
			}
			
		});
		statusPane.add(uie.maxDuration);
		
		boonUiMap.put(cur.getName(), uie);
	}
	
	public void updateStatuses() {
		
	}
	
	public JPanel getStatusPane() {
		return this.statusPane;
	}
}
