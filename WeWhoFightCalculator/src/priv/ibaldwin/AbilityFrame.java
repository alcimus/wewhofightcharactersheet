package priv.ibaldwin;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import priv.ibaldwin.abilities.Ability;

public class AbilityFrame {
	private class abilityUIElements {
		JCheckBox activeCheckbox;
		JTextField curStack;
		JTextField maxStack;
		JButton onHitButton;
		JButton useAbilityButton;
		JButton resetAbilityButton;
	}

	private JTextField hpCur;
	private JTextField maxHp;
	private JTextField percentHp;
	private JTextField manaMax;
	private JTextField manaCur;
	private JTextField percentMp;
	private JTextField stamMax;
	private JTextField stamCur;

	private JTextField percentStam;
	private JButton hpDrainWithResistance;
	private JButton hpFullDrain;
	private JButton mpDrainWithResistance;
	private JButton mpFullDrain;
	private JButton stamDrainWithResistance;
	private JButton stamFullDrain;

	private JPanel abilitiesPane;
	private PlayerCharacter character;
	private MainFrame root;
	private HashMap<String, abilityUIElements> abilityUiMap;

	public AbilityFrame(MainFrame root, PlayerCharacter character) {
		this.character = character;
		this.abilityUiMap = new HashMap<String, abilityUIElements>();
		this.root = root;

		abilitiesPane = new JPanel();
		abilitiesPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		abilitiesPane.setLayout(null);

		// next turn button
		abilitiesPane.add(FrameUtils.makeButton("Next Turn", true, 10, 6, 135, 25, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				character.doUpkeep();
				root.updateOnUpkeep();
				updateOnUpkeep();
			}

		}));

		this.addVatilityTextBoxes();

		for (int i = 0; i < character.getAbilities().size(); i++) {
			Ability cur = character.getAbilities().get(i);
			this.addAbility(i, cur);
		}

		this.addUIHooks();
	}

	private void addVatilityTextBoxes() {
		abilitiesPane.add(FrameUtils.makeLabel("Health", 190, 11, 46, 14));
		hpCur = FrameUtils.makeEnabledTextField(Integer.toString(character.getCurHp()), 246, 10, 56, 20,
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						character.setCurHp(hpCur.getText());

					}

				});
		abilitiesPane.add(hpCur);

		abilitiesPane.add(FrameUtils.makeLabel("/", 309, 11, 15, 14));

		maxHp = FrameUtils.makeEnabledTextField(Integer.toString(character.getMaxHp()), 316, 10, 56, 20,
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						character.setMaxHp(maxHp.getText());

					}

				});
		abilitiesPane.add(maxHp);

		percentHp = FrameUtils.makeDisabledTextField(String.valueOf(character.getPercentHp()) + "%", 388, 7, 67, 23);
		percentHp.setDisabledTextColor(Color.BLACK);
		abilitiesPane.add(percentHp);

		abilitiesPane.add(FrameUtils.makeLabel("Mana", 190, 45, 42, 9));

		manaCur = FrameUtils.makeEnabledTextField(Integer.toString(character.getCurMp()), 246, 36, 56, 20,
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						character.setCurMp(manaCur.getText());

					}

				});
		abilitiesPane.add(manaCur);

		abilitiesPane.add(FrameUtils.makeLabel("/", 309, 37, 15, 14));

		manaMax = FrameUtils.makeEnabledTextField(Integer.toString(character.getMaxMp()), 316, 36, 56, 20,
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						character.setMaxMp(manaMax.getText());

					}

				});
		abilitiesPane.add(manaMax);

		percentMp = FrameUtils.makeDisabledTextField(String.valueOf(character.getPercentMp()) + "%", 388, 36, 67, 20);
		percentMp.setDisabledTextColor(Color.BLACK);
		abilitiesPane.add(percentMp);

		abilitiesPane.add(FrameUtils.makeLabel("Stamina", 190, 67, 42, 9));

		stamCur = FrameUtils.makeEnabledTextField(Integer.toString(character.getCurStam()), 246, 60, 56, 20,
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						character.setCurStam(stamCur.getText());

					}

				});
		abilitiesPane.add(stamCur);

		abilitiesPane.add(FrameUtils.makeLabel("/", 309, 61, 15, 14));

		stamMax = FrameUtils.makeEnabledTextField(Integer.toString(character.getMaxStam()), 319, 61, 56, 20,
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						character.setMaxStam(stamMax.getText());

					}

				});
		abilitiesPane.add(stamMax);

		percentStam = FrameUtils.makeDisabledTextField(String.valueOf(character.getPercentStam()) + "%", 388, 60, 67,
				20);
		percentStam.setDisabledTextColor(Color.BLACK);
		abilitiesPane.add(percentStam);

		hpDrainWithResistance = FrameUtils.makeEnabledButton("0.5%", 461, 7, 67, 23, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				character.enqueVitalityPacket(
						new vitalityPacket("Drain Life", 0.005, 0.0, 0.0, vitalityPacket.PacketType.Drain));
				updateVitatlity();
			}
		});
		abilitiesPane.add(hpDrainWithResistance);

		hpFullDrain = FrameUtils.makeEnabledButton("1.0%", 532, 7, 67, 23, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				character.enqueVitalityPacket(
						new vitalityPacket("Drain Life", 0.01, 0.0, 0.0, vitalityPacket.PacketType.Drain));
				updateVitatlity();
			}
		});
		abilitiesPane.add(hpFullDrain);

		mpDrainWithResistance = FrameUtils.makeEnabledButton("0.5%", 461, 31, 67, 23, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				character.enqueVitalityPacket(
						new vitalityPacket("Drain Mana", 0.0, 0.005, 0.0, vitalityPacket.PacketType.Drain));
				updateVitatlity();
			}
		});
		abilitiesPane.add(mpDrainWithResistance);

		mpFullDrain = FrameUtils.makeEnabledButton("1.0%", 532, 31, 67, 23, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				character.enqueVitalityPacket(
						new vitalityPacket("Drain Mana", 0.0, 0.01, 0.0, vitalityPacket.PacketType.Drain));
				updateVitatlity();
			}
		});
		abilitiesPane.add(mpFullDrain);

		stamDrainWithResistance = FrameUtils.makeEnabledButton("0.5%", 461, 58, 67, 23, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				character.enqueVitalityPacket(
						new vitalityPacket("Drain Stamina", 0.0, 0.0, 0.005, vitalityPacket.PacketType.Drain));
				updateVitatlity();
			}
		});
		abilitiesPane.add(stamDrainWithResistance);

		stamFullDrain = FrameUtils.makeEnabledButton("1.0%", 532, 58, 67, 23, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				character.enqueVitalityPacket(
						new vitalityPacket("Drain Stamina", 0.0, 0.0, 0.01, vitalityPacket.PacketType.Drain));
				updateVitatlity();
			}
		});
		abilitiesPane.add(stamFullDrain);

		updateVitalityColor(character.getPercentHp(), percentHp);
		updateVitalityColor(character.getPercentMp(), percentMp);
		updateVitalityColor(character.getPercentStam(), percentStam);
	}

	private void updateAbilities() {

		for (Ability curAbility : character.getAbilities()) {
			abilityUIElements uie = abilityUiMap.get(curAbility.getName());
			uie.activeCheckbox.setSelected(curAbility.isActive());
			if (curAbility.getDuration() > 0 || curAbility.onCooldown()) {
				uie.curStack.setText(curAbility.getDuration() == 0 ? "" : String.valueOf(curAbility.getDuration()));
				uie.maxStack.setText(curAbility.getCooldown() == 0 ? "" : String.valueOf(curAbility.getCooldown()));
				uie.curStack.setDisabledTextColor(Color.GREEN);
				uie.maxStack.setDisabledTextColor(Color.RED);
			} else {
				uie.curStack.setText(curAbility.getMaxStacks() == 0 ? "" : Integer.toString(curAbility.getCurStacks()));
				uie.maxStack.setText(curAbility.getMaxStacks() == 0 ? "" : Integer.toString(curAbility.getMaxStacks()));
				uie.curStack.setDisabledTextColor(Color.BLACK);
				uie.maxStack.setDisabledTextColor(Color.BLACK);
			}
			uie.useAbilityButton.setEnabled(curAbility.isUsable());
			uie.resetAbilityButton.setEnabled(curAbility.isResettable());
		}

		if (abilityUiMap.get("Blood of the Titans").activeCheckbox.isSelected()) {
			abilityUiMap.get("Blood of the Titans").resetAbilityButton.setEnabled(false);
			abilityUiMap.get("Restoring Presence").resetAbilityButton.setEnabled(false);
			abilityUiMap.get("Armor of the Tempest").resetAbilityButton.setEnabled(false);
			abilityUiMap.get("Hungering Axe").resetAbilityButton.setEnabled(false);
			abilityUiMap.get("Sweeping Winds").resetAbilityButton.setEnabled(false);
		}

	}

	public void updateOnUpkeep() {
		updateVitatlity();
		if (abilityUiMap.get("Blood of the Titans").activeCheckbox.getBackground() == Color.RED) {
			abilityUiMap.get("Blood of the Titans").activeCheckbox
					.setBackground(abilityUiMap.get("Blood of the Titans").onHitButton.getBackground());
			character.disable("Blood of the Titans");
		} else if (abilityUiMap.get("Blood of the Titans").activeCheckbox.isSelected()
				&& character.getPercentMp() < 20.0) {
			abilityUiMap.get("Blood of the Titans").activeCheckbox.setBackground(Color.RED);
		}
		updateAbilities();
	}

	private void updateVitalityColor(double percent, JTextField uiElement) {
		if (percent < 25.0) {
			uiElement.setBackground(Color.RED);
		} else if (percent < 50.0) {
			uiElement.setBackground(Color.YELLOW);
		} else {
			uiElement.setBackground(Color.GREEN);
		}
	}

	private void updateVitatlity() {
		character.HandleVitalityPackets();
		maxHp.setText(String.valueOf(character.getMaxHp()));
		hpCur.setText(String.valueOf(character.getCurHp()));
		percentHp.setText(String.valueOf(character.getPercentHp()) + "%");
		manaMax.setText(String.valueOf(character.getMaxMp()));
		manaCur.setText(String.valueOf(character.getCurMp()));
		percentMp.setText(String.valueOf(character.getPercentMp()) + "%");
		stamMax.setText(String.valueOf(character.getMaxStam()));
		stamCur.setText(String.valueOf(character.getCurStam()));
		percentStam.setText(String.valueOf(character.getPercentStam()) + "%");

		updateVitalityColor(character.getPercentHp(), percentHp);
		updateVitalityColor(character.getPercentMp(), percentMp);
		updateVitalityColor(character.getPercentStam(), percentStam);
	}

	public Component getPane() {
		return abilitiesPane;
	}

	private void addAbility(int index, Ability cur) {
		int yLocation = index * 30 + 100;
		int curStackX = 220;
		int maxStackX = curStackX + 64;
		abilityUIElements uie = new abilityUIElements();

		uie.activeCheckbox = FrameUtils.makeCheckbox(cur.getName(), cur.isActive(), 10, yLocation,
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						if (uie.activeCheckbox.isSelected())
							character.activate(cur.getName());
						else
							cur.disable();
						updateVitatlity();
						updateAbilities();
					}
				});

		abilitiesPane.add(uie.activeCheckbox);

		addStackers(uie, cur, yLocation, curStackX, maxStackX);
		addButtons(uie, cur, yLocation);

		abilityUiMap.put(cur.getName(), uie);
	}

	private void addButtons(abilityUIElements uie, Ability cur, int yLocation) {
		int xLocation = 360;
		if (cur.hasOnHitEffects()) {
			uie.onHitButton = FrameUtils.makeEnabledButton("Hit", xLocation, yLocation, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					character.activateOnHit(cur.getName());
					updateVitatlity();
					updateAbilities();
				}
			});
		} else
			uie.onHitButton = FrameUtils.makeDisabledButton("Hit", xLocation, yLocation);
		abilitiesPane.add(uie.onHitButton);

		if (cur.isUsable()) {
			uie.useAbilityButton = FrameUtils.makeEnabledButton("Use", xLocation + 80, yLocation, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					character.activate(cur.getName());
					updateVitatlity();
					updateAbilities();
				}
			});
		} else
			uie.useAbilityButton = FrameUtils.makeDisabledButton("Use", xLocation + 80, yLocation);

		abilitiesPane.add(uie.useAbilityButton);

		if (cur.isResettable()) {
			uie.resetAbilityButton = FrameUtils.makeEnabledButton("Reset", xLocation + 160, yLocation,
					new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							character.disable(cur.getName());
							updateVitatlity();
							updateAbilities();
						}
					});
		} else {
			uie.resetAbilityButton = FrameUtils.makeDisabledButton("Reset", xLocation + 160, yLocation, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					character.disable(cur.getName());
					updateVitatlity();
					updateAbilities();
				}
			});
		}

		abilitiesPane.add(uie.resetAbilityButton);
	}

	private void addStackers(abilityUIElements uie, Ability cur, int yLocation, int curStackX, int maxStackX) {
		if (cur.getMaxStacks() == 0) {
			uie.curStack = FrameUtils.makeDisabledTextField(curStackX, yLocation);
			uie.maxStack = FrameUtils.makeDisabledTextField(maxStackX, yLocation);
		} else {
			uie.curStack = FrameUtils.makeEnabledTextField(Integer.toString(cur.getCurStacks()), curStackX, yLocation,
					new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent e) {
							cur.setCurStacks(Integer.valueOf(uie.curStack.getText()));
						}
					});

			uie.maxStack = FrameUtils.makeEnabledTextField(Integer.toString(cur.getMaxStacks()), maxStackX, yLocation,
					new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							cur.setMaxStacks(Integer.valueOf(uie.maxStack.getText()));
						}
					});
		}

		abilitiesPane.add(uie.curStack);

		JLabel slash = new JLabel("/");
		slash.setBounds(curStackX + 57, yLocation, 15, 14);
		abilitiesPane.add(slash);

		abilitiesPane.add(uie.maxStack);
	}

	private void addUIHooks() {
		abilityUIElements uie = this.abilityUiMap.get("Blood of the Titans");
		uie.useAbilityButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				character.activate("Restoring Presence");
				character.activate("Armor of the Tempest");
				character.activate("Hungering Axe");
				character.activate("Sweeping Winds");
				updateAbilities();
			}
		});

	}

}
