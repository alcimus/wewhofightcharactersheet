package priv.ibaldwin.Statuses;

import java.util.ArrayList;
import java.util.List;

import priv.ibaldwin.enums.Trait;

public class Status {
	private String name;
	private int durationRemaining;
	private int durationMax;
	private List<Trait> traits;
	
	public Status() {
		this("Default", 0, 0);
	}
	
	public Status(String name, int durationRemaining, int durationMax) {
		this.name = name;
		this.durationRemaining = durationRemaining;
		this.durationMax = durationMax;
		this.traits = new ArrayList<Trait>();
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getDurationRemamining() {
		return durationRemaining;
	}
	
	public int getDurationMax() {
		return durationMax;
	}
	
	public void setDurationMax(int durationMax) {
		this.durationMax = durationMax;
	}
	
	public List<Trait> getTraits(){
		return traits;
	}
}
