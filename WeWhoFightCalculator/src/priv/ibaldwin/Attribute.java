package priv.ibaldwin;

import priv.ibaldwin.enums.Rank;

public class Attribute {
	private Rank rank;
	private int subRank;
	
	public Attribute() {
		this.rank = Rank.UNRANKNED;
		this.subRank = 0;
	}
	
	public Rank getRank() {
		return rank;
	}
	
	public int getRankAsInt() {
		switch(this.rank) {
		case IRON:
			return 1;
		case BRONZE:
			return 2;
		case SILVER:
			return 3;
		case GOLD:
			return 4;
		case DIAMOND:
			return 5;
		default:
			return 0;
		
		}
	}
	
	public int getFullRankAsInt() {
		switch(this.rank) {
		case IRON:
			return 1 + this.subRank;
		case BRONZE:
			return 11 + this.subRank;
		case SILVER:
			return 21 + this.subRank;
		case GOLD:
			return 31 + this.subRank;
		case DIAMOND:
			return 41 + this.subRank;
		default:
			return 0;
		
		}
	}
}
