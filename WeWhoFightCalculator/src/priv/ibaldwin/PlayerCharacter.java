package priv.ibaldwin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import priv.ibaldwin.abilities.Ability;
import priv.ibaldwin.abilities.ApothecarysReserves;
import priv.ibaldwin.abilities.ArmorOfTheTempest;
import priv.ibaldwin.abilities.ArondrightsWrath;
import priv.ibaldwin.abilities.BloodOfTheTitans;
import priv.ibaldwin.abilities.BountifulLife;
import priv.ibaldwin.abilities.HeadmansPursuit;
import priv.ibaldwin.abilities.HungeringAxe;
import priv.ibaldwin.abilities.HungeringCold;
import priv.ibaldwin.abilities.Immortality;
import priv.ibaldwin.abilities.LifesBestowment;
import priv.ibaldwin.abilities.MeleAttack;
import priv.ibaldwin.abilities.MightyStrength;
import priv.ibaldwin.abilities.BrandOfRestoration;
import priv.ibaldwin.abilities.CyclesEmbrace;
import priv.ibaldwin.abilities.FrailtyOfMortality;
import priv.ibaldwin.abilities.GazeOfMortality;
import priv.ibaldwin.abilities.RestoringPresence;
import priv.ibaldwin.abilities.SanguineStrikes;
import priv.ibaldwin.abilities.SweepingWinds;
import priv.ibaldwin.abilities.UnrelentingAssault;
import priv.ibaldwin.abilities.VenomOfTheGreatSerpent;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.vitalityPacket.PacketType;
import priv.ibaldwin.Statuses.Status;

public class PlayerCharacter {
	public static int baseHp = 400;
	public static int maxHp = 400;
	public static int baseMp = 400;
	public static int maxMp = 400;
	public static int baseStam = 400;
	public static int maxStam = 400;
	private int curHp = 390;
	private double maxPercentHp = 1.0;
	private int curMp = 200;
	private double maxPercentMp = 1.0;
	private int curStam = 300;
	private double maxPercentStam = 1.0;
	
	private MainFrame main;
	private List<Ability> abilities;
	private HashMap<String, Ability> abilityNameSet;
	private List<vitalityPacket> queue;
	private List<Status> statuses;
	
	public PlayerCharacter(MainFrame mainFrame) {
		this.main = mainFrame;
		this.queue = new ArrayList<>();
		this.abilities = new ArrayList<Ability>();
		this.abilityNameSet = new HashMap<String, Ability>();
		addAbility(new MeleAttack(this, Rank.BRONZE, 0));
		addAbility(new RestoringPresence(this, Rank.BRONZE, 0));
		addAbility(new HungeringAxe(this, Rank.BRONZE, 0));
		addAbility(new BrandOfRestoration(this, Rank.BRONZE, 0));
		addAbility(new LifesBestowment(this, Rank.BRONZE, 0));
		addAbility(new HeadmansPursuit(this, Rank.IRON, 9));
		addAbility(new MightyStrength(this, Rank.BRONZE, 0));
		addAbility(new BloodOfTheTitans(this, Rank.BRONZE, 0));
		addAbility(new CyclesEmbrace(this, Rank.BRONZE, 0));
		addAbility(new ArmorOfTheTempest(this, Rank.BRONZE, 0));
		addAbility(new VenomOfTheGreatSerpent(this, Rank.BRONZE, 0));
		addAbility(new UnrelentingAssault(this, Rank.BRONZE, 0));
		addAbility(new SanguineStrikes(this, Rank.BRONZE, 0));
		addAbility(new SweepingWinds(this, Rank.BRONZE, 0));
		addAbility(new ArondrightsWrath(main, this, Rank.BRONZE, 0));
		addAbility(new BountifulLife(this, Rank.BRONZE, 0));
		addAbility(new Immortality(this, Rank.BRONZE, 0));
		addAbility(new HungeringCold(this, Rank.BRONZE, 0));
		addAbility(new GazeOfMortality(this, Rank.BRONZE, 0));
		addAbility(new FrailtyOfMortality(this, Rank.BRONZE, 0));
		addAbility(new ApothecarysReserves(this, Rank.BRONZE, 0));
		
		this.statuses = new ArrayList<Status>();
		this.statuses.add(new Status());
		this.statuses.add(new Status());
		this.statuses.add(new Status());
		this.statuses.add(new Status());
		this.statuses.add(new Status());
		this.statuses.add(new Status());
		this.statuses.add(new Status());
	}
	
	public void activate(String abilityName) {
		if(!this.abilityNameSet.get(abilityName).isActive()) {
			main.print("Activating "+ abilityName);
			this.abilityNameSet.get(abilityName).activate();
			this.HandleVitalityPackets();
		}
		else {
			main.print(abilityName + " already activated, skipping");
		}
	}

	public void activateOnHit(String abilityName) {
		main.print("On Hit effects for " + abilityName);
		this.abilityNameSet.get(abilityName).onHit();
		this.HandleVitalityPackets();
	}
	
	private void addAbility(Ability cur) {
		abilities.add(cur);
		abilityNameSet.put(cur.getName(), cur);
	}
	
	public void disable(String abilityName) {
		main.print("Disabling " + abilityName);
		this.abilityNameSet.get(abilityName).disable();
	}
	
	public void doUpkeep() {
		main.print("-----------------------------------------------");
		main.print("Start Upkeep" + curHp + " " + curMp + " " + curStam);
		this.enqueVitalityPacket(new vitalityPacket("Recovery", 0.02, 0.04, 0.02, PacketType.Recovery));
		for(Ability cur : abilities) {
			cur.doUpkeep();
		}
		this.HandleVitalityPackets();
		main.print("End Upkeep " + curHp + " " + curMp + " " + curStam);
		main.print("-----------------------------------------------");
		for(Ability cur : abilities) {
			cur.postUpkeep();
		}
		this.HandleVitalityPackets();
		main.print("Post Upkeep " + curHp + " " + curMp + " " + curStam);
		main.print("-----------------------------------------------");
		
		
	}
	
	public void enqueVitalityPacket(vitalityPacket packet) {
		this.queue.add(packet);
	}
	
	public List<Ability> getAbilities(){
		return this.abilities;
	}
	
	public List<Status> getStatuses(){
		return this.statuses;
	}
	
	public int getCurHp() {
		return curHp;
	}
	
	public int getCurMp() {
		return curMp;
	}
	
	public int getCurStam() {
		return curStam;
	}
	
	public int getMaxHp() {
		return maxHp;
	}
	
	public int getMaxMp() {
		return maxMp;
	}
	
	public double getMaxPercentHp() {
		return maxPercentHp;
	}
	
	public double getMaxPercentMp() {
		return maxPercentMp;
	}
	
	public double getMaxPercentStam() {
		return maxPercentStam;
	}
	
	public int getMaxStam() {
		return maxStam;
	}
	
	public double getPercentHp() {
		return Math.floor(((double) this.getCurHp()) / ((double) this.getMaxHp()) * 100.0);
	}
	
	public double getPercentMp() {
		return Math.floor(((double) this.getCurMp()) / ((double) this.getMaxMp()) * 100.0);
	}
	
	public double getPercentStam() {
		return Math.floor(((double) this.getCurStam()) / ((double) this.getMaxStam()) * 1000.0)/10.0;
	}
	
	private void handlePacket(vitalityPacket vp) {
		if(vp == null)
			return;
		main.print("got packet\n"+ vp.toString());
		
		for(Ability cur : this.abilities) {
			cur.modifyPacket(vp);
		}
		main.print("post abilities\n"+ vp.toString());
		
		int initialHp = this.curHp;
		int initialMp = this.curMp;
		int initialStam = this.curStam;
		
		if(curHp < maxHp)
			curHp = curHp + vp.getTotalHp() < maxHp ? curHp + vp.getTotalHp() : maxHp;
		this.setCurHp(this.curHp);
		
		if(curMp < maxMp)
			curMp = curMp + vp.getTotalMp() < maxMp ? curMp + vp.getTotalMp() : maxMp;
		this.setCurMp(this.curMp);
		
		if(curStam < maxStam)
			curStam = curStam + vp.getTotalStam() < maxStam ? curStam + vp.getTotalStam() : maxStam;
		this.setCurStam(this.curStam);
		
		for(Ability cur : this.abilities) {
			cur.handlePacket(vp, initialHp, initialMp, initialStam);
		}
		
		main.print("new vit\n" + curHp + " " + curMp + " " + curStam);
	}
	
	public void HandleVitalityPackets() {
		while(!this.queue.isEmpty()) {
			this.handlePacket(queue.remove(0));
		}
	}
	
	public void setCurHp(int hp) {
		curHp = Integer.min((int) (maxHp * maxPercentHp), hp);
		if(curHp < 0) curHp = 0;
	}
	
	public void setCurHp(String text) {
		curHp = Integer.parseInt(text);
	}
	
	public void setCurMp(int mp) {
		curMp = Integer.min((int) (maxMp * maxPercentMp), mp);
		if(curMp < 0) curMp = 0;
	}
	
	public void setCurMp(String text) {
		curMp = Integer.parseInt(text);
	}
	
	public void setCurStam(int stam) {
		curStam = Integer.min((int) (maxStam * maxPercentStam), stam);
		if(curStam < 0) curStam = 0;
	}
	
	public void setCurStam(String text) {
		curStam = Integer.parseInt(text);
	}

	public void setMaxHp(String text) {
		maxHp = Integer.parseInt(text);
	}

	public void setMaxMp(String text) {
		maxMp = Integer.parseInt(text);
	}

	public void setMaxPercentHp(double maxPercentHp) {
		this.maxPercentHp = maxPercentHp;
	}

	public void setMaxPercentMp(double maxPercentMp) {
		this.maxPercentMp = maxPercentMp;
	}

	public void setMaxPercentStam(double maxPercentStam) {
		this.maxPercentStam = maxPercentStam;
	}

	public void setMaxStam(String text) {
		maxStam = Integer.parseInt(text);
	}
	
	public void log(String s) {
		main.print(s);
	}
}
