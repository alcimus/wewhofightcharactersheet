package priv.ibaldwin;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

public class MainFrame extends JFrame {
	
	
	private static final long serialVersionUID = 1L;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
					frame.setTitle("We Who Fight Character Calculator");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	private AbilityFrame abilitiesPane; 
	private PlayerCharacter character;
	private JTabbedPane tabbedPane;
	private StatusFrame statusPane;
	
	private LogFrame logPane;

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		this.character = new PlayerCharacter(this);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1142, 840);
		setResizable(false);
		tabbedPane = new JTabbedPane();
		setContentPane(tabbedPane);
		
		abilitiesPane = new AbilityFrame(this, character);
		tabbedPane.addTab("Abilities", abilitiesPane.getPane());
		statusPane = new StatusFrame(this, character);
		tabbedPane.addTab("Statuses", statusPane.getStatusPane());
		logPane = new LogFrame();
		tabbedPane.addTab("Log", logPane.getLogPane());
		
	}
	
	
	
	public void updateOnUpkeep() {
		abilitiesPane.updateOnUpkeep();
		statusPane.updateStatuses();
	}
	
	public void print(String c) {
		logPane.print(c);
	}
	
}
