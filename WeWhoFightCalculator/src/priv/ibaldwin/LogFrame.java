package priv.ibaldwin;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

public class LogFrame {
	private JPanel logPane;
	private JTextArea textArea;
	
	public LogFrame(){
		logPane = new JPanel();
		logPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		logPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 1081, 684);
		logPane.add(scrollPane);
		
		//adding log
		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		textArea.setEditable(false);
	}
	
	public void updateOnUpkeep() {
		
	}
	
	public JPanel getLogPane() {
		return this.logPane;
	}
	
	public void print(String c) {
		textArea.append(c + "\n");
	}


}
