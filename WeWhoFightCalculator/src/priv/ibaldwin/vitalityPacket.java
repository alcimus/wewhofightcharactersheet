package priv.ibaldwin;

public class vitalityPacket {
	private String source;
	private PacketType type;
	private double percentHp;
	private double percentMp;
	private double percentStam;
	private int rawHp; 
	private int rawMp;
	private int rawStam;
	
	public enum PacketType {
		Healing,
		Drain,
		Recovery,
		Cost,
	}
	
	public vitalityPacket(String source, double percentHp, int rawHp, double percentMp, int rawMp, double percentStam, int rawStam, PacketType type) {
		this.source = source;
		this.percentHp = percentHp;
		this.percentMp = percentMp;
		this.percentStam = percentStam;
		this.rawHp = rawHp;
		this.rawMp = rawMp;
		this.rawStam = rawStam;
		this.type = type;
	}
	
	public vitalityPacket(String source, int rawHp, int rawMp, int rawStam, PacketType type) {
		this(source, 0.0, rawHp, 0.0, rawMp, 0.0, rawStam, type);
	}
	
	public vitalityPacket(String source, double percentHp, double percentMp, double percentStam, PacketType type) {
		this(source, percentHp, 0, percentMp,0,percentStam, 0, type);
	}
	
	public vitalityPacket(String source, double hp, double mp, double stam) {
		this(source, hp, mp, stam, null);
	}
	
	public vitalityPacket() {
		this("", 0.0,0.0,0.0, PacketType.Recovery);
	}
	
	public boolean isHealing() {
		return this.type == PacketType.Healing || this.type == PacketType.Drain;
	}
	
	public boolean isDrain() {
		return this.type == PacketType.Drain;
	}
	
	public boolean isRecovery() {
		return this.type == PacketType.Recovery;
	}
	
	public double getPercentHp() {
		return this.percentHp;
	}
	
	public double getPercentMp() {
		return this.percentMp;
	}
	
	public double getPercentStam() {
		return this.percentStam;
	}
	
	public int getRawHp() {
		return rawHp;
	}
	
	public int getRawMp() {
		return rawMp;
	}
	
	public int getRawStam() {
		return rawStam;
	}
	
	public int getTotalHp() {
		return rawHp + (percentHp < 0 ? (int) Math.floor((double) PlayerCharacter.baseHp * this.percentHp) : (int) Math.floor((double) PlayerCharacter.maxHp * this.percentHp));
	}
	
	public int getTotalMp() {
		return rawMp + (percentMp < 0 ? (int) Math.floor((double) PlayerCharacter.baseMp * this.percentMp) : (int) Math.floor((double) PlayerCharacter.maxMp * this.percentMp));
	}
	
	public int getTotalStam() {
		return rawStam + (percentStam < 0 ? (int) Math.floor((double) PlayerCharacter.baseStam * this.percentStam) : (int) Math.floor((double) PlayerCharacter.maxStam * this.percentStam));
	}
	
	public String toString() {
		String ret = "Source: " + this.source + " ";
		ret += "percents: ["+ this.getPercentHp() + ", " + this.getPercentMp() + ", " + this.getPercentStam() + "] ";
		ret += "raw: ["+ this.getRawHp() + ", " + this.getRawMp() + ", " + this.getRawStam() + "] ";
		ret += "total: ["+ this.getTotalHp() + ", " + this.getTotalMp() + ", " + this.getTotalStam() + "] ";
		ret += "type: "+ this.type;
		
		return ret;
	}
	
	public void sumPackets(vitalityPacket a) {
		if(a == null) {
			return;
		}
		
		this.percentHp = this.percentHp + a.percentHp;
		this.percentMp = this.percentMp + a.percentMp;
		this.percentStam = this.percentStam + a.percentStam;
	}
	
	public void convertToRaw() {
		this.rawHp = this.getTotalHp();
		this.rawMp = this.getTotalMp();
		this.rawStam = this.getTotalStam();
		this.percentHp = 0.0;
		this.percentMp = 0.0;
		this.percentStam = 0.0;
	}
	
	
	public void addPercent(vitalityPacket a) {
		this.percentHp += a.getPercentHp(); 
		this.percentMp += a.getPercentMp();
		this.percentStam += a.getPercentStam();
	} 
	
	public vitalityPacket addRaw(vitalityPacket a) {
		vitalityPacket vp = new vitalityPacket(this.source, a.getRawHp() + this.getRawHp(), a.getRawMp() + this.getRawMp(), a.getRawStam() + this.getRawStam(), this.type);
		return vp;
	}
	
	public void addRaw(int hp, int mp, int stam) {
		this.rawHp += hp;
		this.rawMp += mp;
		this.rawStam += stam;
	}
	
	public void multiply(double multiplier) {
		multiply(multiplier, multiplier, multiplier);
	}
	
	public void multiply(double hpMultiplier, double mpMultiplier, double stamMultiplier) {
		this.percentHp = hpMultiplier * this.getPercentHp();
		this.rawHp = (int) (hpMultiplier * this.getRawHp());
		this.percentMp = mpMultiplier * this.getPercentMp(); 
		this.rawMp = (int) (mpMultiplier * this.getRawMp()); 
		this.percentStam = stamMultiplier * this.getPercentStam(); 
		this.rawStam = (int) (stamMultiplier * this.getRawStam());
	}
	
	public String getSource() {
		return this.source;
	}
}
