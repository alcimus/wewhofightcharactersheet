package priv.ibaldwin.enums;

public enum Trait {
	SpecialAbility, // These are abilities that dont require vocal components and grant extra
					// abilities. These can be either passive or active (or both)
	SpecialAttack, // these augment attacks, granting additional power and affects while doing an
					// attack
	Spell, // these are abilities that grant you an innate understanding of a spell and
			// make the casting of the spell easier
	Movement, // these abilities all enhance movement in some way, increasing movement speed,
				// granting ability to move over difficult terrain, or flight
	Drain, // Drains a resource from a target
	Conjuration, // creates something
	Execute, // scales damage based on a depleted resource, either yours or the target's
	Holy, // divine in nature, tends to be affiliated with Life and sanctification
	Unholy, // corrupting in nature, tends to be affiliated with Undeath or defiling
	Affliction, // leaves a lasting affect on the target
	Disease, // leaves a disease on the target
	Curse, // curses the target
	Magic, // causes a magical affect on the target (cant be explained)
	Perception, // Ability that grants enhanced ocular prowess
	Aura, // AOE projection of the soul, grants boons to allies and/or negative affects to
			// enemies
	Boon, // A beneficial buff, can be stacking
	Combination, // can be used in concert with other abilities
	Stacking, // when applied multiple times has a growing affect, either in power or duration
	Dimension, // involves manipulating the fabric of reality, these tend to be either storage,
				// portal, or teleport abilities
	Life, // involves healing or the magical essence of life, tends to help the living and
			// can hurt the undead
	Restoration, // involves restoring HP, MP, or Stamina over time but not a direct magical
					// heal, tends to be an over time healing affect
	Healing, // A short term effect that restores HP
	Blood, // most of these abilities are draining and healing type abilities, and many
			// need to be used on a creature with blood
	Wind, // uses the movement of air to accomplish its affect, can include flying affects
}
