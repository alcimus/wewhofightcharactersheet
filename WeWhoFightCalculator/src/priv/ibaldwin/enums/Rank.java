package priv.ibaldwin.enums;

public enum Rank {
	UNRANKNED,
	IRON,
	BRONZE,
	SILVER,
	GOLD,
	DIAMOND
}
