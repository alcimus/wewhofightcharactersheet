package priv.ibaldwin.abilities;

import priv.ibaldwin.PlayerCharacter;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;

public class VenomOfTheGreatSerpent extends Ability{
	
	public VenomOfTheGreatSerpent(PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank);
		super.addTrait(Trait.Conjuration);
		super.addTrait(Trait.Dimension);
		super.addTrait(Trait.SpecialAbility);
	}
	
	@Override
	public AbilityType getAbilityType() {
		return AbilityType.SpecialAbilityPassive;
	}

	@Override
	public String getName() {
		return "Venom of the Great Serpent";
	}
	
	@Override
	public boolean isActive() {
		return true;
	}
	
	@Override
	public void disable() {
		
	}
}
