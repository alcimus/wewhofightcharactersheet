package priv.ibaldwin.abilities;

import priv.ibaldwin.PlayerCharacter;
import priv.ibaldwin.vitalityPacket;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;
import priv.ibaldwin.vitalityPacket.PacketType;

public class HungeringAxe extends Ability{
	
	public HungeringAxe(PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank);
		super.addTrait(Trait.Conjuration);
		super.addTrait(Trait.SpecialAbility);
		super.addTrait(Trait.Drain);
		switch(super.getRank()) {
		default:
		case UNRANKNED:
		case IRON:
			super.setMaxStacks(0);
			break;
		case BRONZE:
			super.setMaxStacks(20);
			break;
		case SILVER:
			super.setMaxStacks(30);
			break;
		case GOLD:
			super.setMaxStacks(40);
			break;
		case DIAMOND:
			super.setMaxStacks(50);
			break;
		}
	}
	
	@Override
	public void activate() {
		super.activate();
		super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.0, -0.05, 0.0, PacketType.Cost));
	}
	
	@Override
	public void doUpkeep() {
		if(super.isActive())
			super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.0, -0.025, 0.0, PacketType.Cost));
	}
	
	@Override
	public AbilityType getAbilityType() {
		return AbilityType.SpecialAbilityActive;
	}

	@Override
	public boolean isOngoing() {
		return true;
	}

	@Override
	public String getName() {
		return "Hungering Axe";
	}

	@Override
	public boolean hasOnHitEffects() {
		return true;
	}
	
	@Override
	public void onHit() {
		super.incrementStacks();
	}
	
	@Override
	public void modifyPacket(vitalityPacket packet) {
		if(packet.isDrain()) {
			packet.multiply(1.0+0.05*super.getCurStacks());
		}
	}
	
}
