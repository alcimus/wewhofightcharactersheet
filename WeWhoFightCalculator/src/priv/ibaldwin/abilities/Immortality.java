package priv.ibaldwin.abilities;

import priv.ibaldwin.vitalityPacket;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;
import priv.ibaldwin.vitalityPacket.PacketType;
import priv.ibaldwin.PlayerCharacter;

public class Immortality extends Ability{
	private int ongoingRestoration;
	
	public Immortality(PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank, 0, 24*60*60, 0, 0);
		super.addTrait(Trait.Conjuration);
		super.addTrait(Trait.Dimension);
		super.addTrait(Trait.SpecialAbility);
	}
	
	@Override
	public void doUpkeep() {
		if(super.getDuration() > 0) {
			System.out.println("restoring " + ongoingRestoration);
			super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), ongoingRestoration, ongoingRestoration, ongoingRestoration, vitalityPacket.PacketType.Recovery));
		}
		super.doUpkeep();
	}
	
	@Override
	public void activate() {
		super.startCooldown();
		
		//iron rank
		int hpRecovered = (int) (super.getCharacter().getMaxHp() - super.getCharacter().getCurHp())/ 2;
		int mpRecovered = (int) (super.getCharacter().getMaxMp() - super.getCharacter().getCurMp())/ 2;
		int stamRecovered = (int) (super.getCharacter().getMaxStam() - super.getCharacter().getCurStam())/ 2;
		
		super.getCharacter().enqueVitalityPacket(
				new vitalityPacket(
						this.getName(),
						hpRecovered, 
						mpRecovered, 
						stamRecovered, 
						PacketType.Healing
						)
				);
		
		//bronze rank
		super.setDuration(30);
		this.ongoingRestoration = (int) ((
					((double) hpRecovered) / ((double) super.getCharacter().getMaxHp()) + 
					((double) mpRecovered) / ((double) super.getCharacter().getMaxMp()) + 
					((double) stamRecovered) / ((double) super.getCharacter().getMaxStam())
				) * 100.0); 
	}

	@Override
	public AbilityType getAbilityType() {
		return AbilityType.SpecialAbilityActive;
	}

	@Override
	public String getName() {
		return "Immortality";
	}
}
