package priv.ibaldwin.abilities;

import priv.ibaldwin.PlayerCharacter;
import priv.ibaldwin.vitalityPacket;
import priv.ibaldwin.vitalityPacket.PacketType;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;

public class FrailtyOfMortality extends Ability{
	
	public FrailtyOfMortality(PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank);
		super.addTrait(Trait.SpecialAbility);
		super.addTrait(Trait.Combination);
		super.addTrait(Trait.Affliction);
	}
	
	@Override
	public void activate() {
		super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.0, 0.0, -0.1, PacketType.Cost));
	}

	@Override
	public AbilityType getAbilityType() {
		return AbilityType.SpecialAbilityActive;
	}

	@Override
	public String getName() {
		return "Frailty of Mortality";
	}

}
