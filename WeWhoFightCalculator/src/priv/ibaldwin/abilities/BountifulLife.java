package priv.ibaldwin.abilities;

import priv.ibaldwin.PlayerCharacter;
import priv.ibaldwin.vitalityPacket;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;

public class BountifulLife extends Ability{
	public BountifulLife(PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank);
		super.addTrait(Trait.Life);
		super.addTrait(Trait.SpecialAbility);
		character.setMaxPercentHp(character.getMaxPercentHp() + 0.5);
	}
	
	@Override
	public void postUpkeep() {
		super.postUpkeep();
		if(super.getCharacter().getCurHp() > super.getCharacter().getMaxHp()) {
			
			super.getCharacter().setCurHp(Integer.max(
					super.getCharacter().getCurHp() - (int) Math.ceil(super.getCharacter().getMaxHp() * 0.02), 
					super.getCharacter().getMaxHp()
					)
				);
		}
	}
	
	@Override
	public void handlePacket(vitalityPacket packet, int initialHp, int initialMp, int initialStam) {
		if(super.getCharacter().getCurHp() != initialHp + packet.getRawHp() && packet.isHealing()) {
			super.getCharacter().setCurHp(Integer.min(initialHp + packet.getRawHp(), (int) ((double) super.getCharacter().getMaxHp() * 1.50)));
		}
	}
	
	
	@Override
	public void modifyPacket(vitalityPacket packet) {
		if(packet.isHealing()) {
			packet.addRaw((int) (0.05 * (double) super.getRankAsInt() * (double) packet.getTotalHp()), 0, 0);
		}
	}

	@Override
	public void disable() {
		
	}

	@Override
	public AbilityType getAbilityType() {
		return AbilityType.SpecialAbilityPassive;
	}

	@Override
	public String getName() {
		return "Bountiful Life";
	}

	@Override
	public boolean isActive() {
		return true;
	}
}
