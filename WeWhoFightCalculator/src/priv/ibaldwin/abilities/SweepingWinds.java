package priv.ibaldwin.abilities;

import priv.ibaldwin.PlayerCharacter;
import priv.ibaldwin.vitalityPacket;
import priv.ibaldwin.vitalityPacket.PacketType;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;

public class SweepingWinds extends Ability{
	
	public SweepingWinds(PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank);
		super.addTrait(Trait.SpecialAbility);
		super.addTrait(Trait.Wind);
		super.setMaxStacks(10);
	}
	
	@Override
	public void activate() {
		super.activate();
		super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.0, -0.04, 0.0));
	}
	
	@Override
	public void disable() {
		super.setCurStacks(0);
		super.disable();
	}
	
	@Override
	public void doUpkeep() {
		if(super.isActive()) {
			super.incrementStacks();
			super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.0, -0.02, 0.0, PacketType.Cost));
		}
	}

	@Override
	public AbilityType getAbilityType() {
		return AbilityType.SpecialAbilityActive;
	}

	@Override
	public boolean isOngoing() {
		return true;
	}

	@Override
	public String getName() {
		return "Sweeping Winds";
	}
}
