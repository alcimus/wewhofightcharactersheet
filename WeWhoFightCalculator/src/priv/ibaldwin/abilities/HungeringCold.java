package priv.ibaldwin.abilities;

import priv.ibaldwin.PlayerCharacter;
import priv.ibaldwin.vitalityPacket;
import priv.ibaldwin.vitalityPacket.PacketType;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;

public class HungeringCold extends Ability{
	
	public HungeringCold(PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank);
		super.addTrait(Trait.SpecialAbility);
		super.addTrait(Trait.Drain);
	}
	
	@Override
	public void activate() {
		super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.0,-0.02, 0.0, PacketType.Cost));
	}

	@Override
	public void onHit() {
		super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.05,0.04, 0.0, PacketType.Healing));
	}

	@Override
	public AbilityType getAbilityType() {
		return AbilityType.SpecialAbilityActive;
	}

	@Override
	public String getName() {
		return "Hungering Cold";
	}
	
	@Override
	public boolean hasOnHitEffects() {
		return true;
	}

}
