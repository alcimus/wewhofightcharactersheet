package priv.ibaldwin.abilities;

import priv.ibaldwin.PlayerCharacter;
import priv.ibaldwin.vitalityPacket;
import priv.ibaldwin.vitalityPacket.PacketType;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;

public class BrandOfRestoration extends Ability{
	
	public BrandOfRestoration(PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank, 0, 120, 0, 0);
		super.addTrait(Trait.SpecialAttack);
		super.addTrait(Trait.Combination);
		super.addTrait(Trait.Healing);
		super.addTrait(Trait.Life);
	}
	
	@Override
	public void activate() {
		super.startCooldown();
		super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.0, -0.10, -0.10, PacketType.Cost));
		super.setDuration(60);
	}

	@Override
	public void onHit() {
		super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.01, 0.0, 0.0, PacketType.Healing));
		super.setDuration(60);
	}

	@Override
	public AbilityType getAbilityType() {
		return AbilityType.SpecialAttack;
	}

	@Override
	public String getName() {
		return "Brand of Restoration";
	}

	@Override
	public boolean hasOnHitEffects() {
		return true;
	}

}
