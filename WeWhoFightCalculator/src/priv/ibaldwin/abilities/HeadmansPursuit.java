package priv.ibaldwin.abilities;

import priv.ibaldwin.PlayerCharacter;
import priv.ibaldwin.vitalityPacket;
import priv.ibaldwin.vitalityPacket.PacketType;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;

public class HeadmansPursuit extends Ability{
	
	public HeadmansPursuit(PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank);
		super.addTrait(Trait.SpecialAbility);
		super.addTrait(Trait.Conjuration);
	}
	
	@Override
	public void activate() {
		super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.0, -0.02, 0.0, PacketType.Cost));
	}

	@Override
	public void onHit() {
		super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.0, 0.01, 0.01, PacketType.Drain));
	}

	@Override
	public AbilityType getAbilityType() {
		return AbilityType.SpecialAttack;
	}

	@Override
	public String getName() {
		return "Headman's Pursuit";
	}

	@Override
	public boolean hasOnHitEffects() {
		return true;
	}

}
