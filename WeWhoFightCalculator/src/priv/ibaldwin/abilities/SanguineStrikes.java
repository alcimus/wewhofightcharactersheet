package priv.ibaldwin.abilities;

import priv.ibaldwin.PlayerCharacter;
import priv.ibaldwin.vitalityPacket;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;

public class SanguineStrikes extends Ability{
	public SanguineStrikes(PlayerCharacter character ,Rank rank, int subRank) {
		super(character, rank, subRank, 0, 60, 0, 0);
		super.addTrait(Trait.SpecialAttack);
		super.addTrait(Trait.Combination);
		super.addTrait(Trait.Blood);
	}
	
	@Override
	public void activate() {
		super.startCooldown();
		super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.0, 0.0, -0.15));
	}

	@Override
	public AbilityType getAbilityType() {
		return AbilityType.SpecialAttack;
	}

	@Override
	public String getName() {
		return "Sanguine Strikes";
	}
	
}
