package priv.ibaldwin.abilities;

import priv.ibaldwin.vitalityPacket;
import priv.ibaldwin.vitalityPacket.PacketType;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;
import priv.ibaldwin.PlayerCharacter;

public class BloodOfTheTitans extends Ability{
	
	public BloodOfTheTitans(PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank);
		super.addTrait(Trait.SpecialAbility);
	}
	
	@Override
	public void doUpkeep() {
		super.doUpkeep();
		if(super.isActive())
			super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.0, -0.05, 0.0, PacketType.Cost));
	}
	
	@Override
	public void activate() {
		super.activate();
		super.getCharacter().enqueVitalityPacket( new vitalityPacket(this.getName(), 0.0, -0.1, 0.00, PacketType.Cost));
	}
	
	@Override
	public void disable() {
		if(super.getCharacter().getPercentMp() < 20)
			super.disable();
	}
	
	@Override
	public AbilityType getAbilityType() {
		return AbilityType.SpecialAbilityActive;
	}

	@Override
	public boolean isOngoing() {
		return true;
	}

	@Override
	public String getName() {
		return "Blood of the Titans";
	}

}
