package priv.ibaldwin.abilities;

import priv.ibaldwin.PlayerCharacter;
import priv.ibaldwin.vitalityPacket;
import priv.ibaldwin.vitalityPacket.PacketType;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;

public class RestoringPresence extends Ability{
	
	public RestoringPresence(PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank);
		super.addTrait(Trait.SpecialAbility);
		super.addTrait(Trait.Aura);
		super.addTrait(Trait.SpecialAbility);
	}
	
	@Override
	public void doUpkeep() {
		if(super.isActive()) {
			super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.0, -0.015, 0.0, PacketType.Cost));
			super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.01, 0.0, 0.0, PacketType.Healing));
		}
	}
	
	@Override
	public void activate() {
		super.activate();
		super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.0, -0.05, 0.0, PacketType.Cost));
	}
	
	@Override
	public AbilityType getAbilityType() {
		return AbilityType.SpecialAbilityActive;
	}

	@Override
	public boolean isOngoing() {
		return true;
	}

	@Override
	public String getName() {
		return "Restoring Presence";
	}
}
