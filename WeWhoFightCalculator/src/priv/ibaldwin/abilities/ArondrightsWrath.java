package priv.ibaldwin.abilities;

import priv.ibaldwin.MainFrame;
import priv.ibaldwin.vitalityPacket;
import priv.ibaldwin.vitalityPacket.PacketType;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;
import priv.ibaldwin.PlayerCharacter;

public class ArondrightsWrath extends Ability{
	private MainFrame main;
	
	public ArondrightsWrath(MainFrame main, PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank, 0, 600, 0, 0);
		this.main = main;
		super.addTrait(Trait.Conjuration);
		super.addTrait(Trait.Dimension);
		super.addTrait(Trait.Spell);
	}
	
	@Override
	public void activate() {
		double missingMp = (double) super.getCharacter().getMaxMp() - ((double) super.getCharacter().getCurMp() - PlayerCharacter.baseMp * 0.1);
		double missingMpPercent = (missingMp /((double) super.getCharacter().getMaxMp())) * 100.0;
		main.print("Arondright's " + missingMpPercent + "% MP missing, Damage: " + (missingMpPercent * 0.01 * super.getCharacter().getMaxHp()));
		super.startCooldown();
		super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.0, -0.1, 0.0, PacketType.Cost));
	}

	@Override
	public AbilityType getAbilityType() {
		return AbilityType.SpecialAbilityActive;
	}

	@Override
	public String getName() {

		return "Arondright's Wrath";
	}
}
