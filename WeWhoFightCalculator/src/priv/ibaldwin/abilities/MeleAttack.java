package priv.ibaldwin.abilities;

import priv.ibaldwin.PlayerCharacter;
import priv.ibaldwin.enums.Rank;

public class MeleAttack extends Ability{
	
	public MeleAttack(PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank);
	}
	
	@Override
	public String getName() {
		return "Melee Attack";
	}

	@Override
	public AbilityType getAbilityType() {
		return AbilityType.NormalAttack;
	}
	
	@Override
	public void activate() {
		
	}

}
