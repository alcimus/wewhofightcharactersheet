package priv.ibaldwin.abilities;

import java.util.ArrayList;
import java.util.List;

import priv.ibaldwin.PlayerCharacter;
import priv.ibaldwin.vitalityPacket;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;

public class Ability {
	
	public enum AbilityType {
		NormalAttack, SpecialAbilityActive, SpecialAbilityPassive, SpecialAttack, Spell
	}
	private PlayerCharacter character;
	private Rank rank;
	private int subRank;
	private List<Trait> traits;
	private int cooldownRemaining;
	private int maxCooldown;
	private int stacksCur;
	private int stacksMax;
	private int duration;
	private boolean active;
	
	public Ability() {
		this(null, Rank.UNRANKNED, 0, 0, 0, 0, 0);
	}
	
	public Ability(PlayerCharacter character, Rank rank, int subRank) {
		this(character, rank, subRank, 0, 0, 0, 0);
	}

	public Ability(PlayerCharacter character, Rank rank, int subRank, int cooldownRemianing, int maxCooldown, int stacksCur, int stacksMax) {
		this.character = character;
		this.rank = rank;
		this.subRank = subRank;
		this.traits = new ArrayList<>();
		this.cooldownRemaining = cooldownRemianing;
		this.maxCooldown = maxCooldown;
		this.stacksCur = stacksCur;
		this.stacksMax = stacksMax;
		this.active = false;
	}

	public void activate() {
		this.active = true;
	}

	public void addTrait(Trait trait) {
		this.traits.add(trait);
	}

	public void disable() {
		this.active = false;
	}
	
	public void doUpkeep() {
		cooldownRemaining = cooldownRemaining - 6 < 0 ? 0 : cooldownRemaining - 6;
		duration = duration - 6 < 0 ? 0 : duration - 6;
	}
	
	public AbilityType getAbilityType() {
		return null;
	}
	
	public boolean isResettable() {
		switch (this.getAbilityType()) {
		case NormalAttack:
			return false;
		case SpecialAbilityActive:
			return this.isActive();
		case SpecialAbilityPassive:
			return false;
		case SpecialAttack:
			return true;
		case Spell:
			return true;
		default:
			return false;
		}
	}
	public boolean isUsable() {
		return !(this.getAbilityType() == AbilityType.SpecialAbilityPassive || this.isActive()) && !this.onCooldown();
	}

	public PlayerCharacter getCharacter() {
		return this.character;
	}
	
	public int getCooldown() {
		return cooldownRemaining;
	}

	public int getCurStacks() {
		return stacksCur;
	}

	public int getDuration() {
		return this.duration;
	}

	public int getFullRank() {
		switch(this.rank) {
		case IRON:
			return 1 + this.subRank;
		case BRONZE:
			return 11 + this.subRank;
		case SILVER:
			return 21 + this.subRank;
		case GOLD:
			return 31 + this.subRank;
		case DIAMOND:
			return 41 + this.subRank;
		default:
			return 0;
		
		}
	}

	public int getMaxStacks() {
		return stacksMax;
	}

	public String getName() {
		return "Default Ability";
	}

	public Rank getRank() {
		return this.rank;
	}

	public int getRankAsInt() {
		switch(this.rank) {
		case UNRANKNED:
		default:
			return 0;
		case IRON:
			return 1;
		case BRONZE:
			return 2;
		case SILVER:
			return 3;
		case GOLD:
			return 4;
		case DIAMOND:
			return 5;
		}
	}

	public List<Trait> getTraits(){
		return this.traits;
	}

	public void handlePacket(vitalityPacket packet, int initialHp, int initialMp, int initialStam) {
		
	}

	public boolean hasOnHitEffects() {
		return false;
	}

	public void incrementStacks() {
		this.incrementStacks(1);
	}
	
	public void incrementStacks(int count) {
		this.stacksCur = this.stacksCur + count < this.stacksMax? this.stacksCur + count: this.stacksMax;
		
	}
	
	public boolean isActive() {
		return active;
	}
	
	public boolean isOngoing() {
		return false;
	}

	public void modifyPacket(vitalityPacket packet) {
	}

	public boolean onCooldown() {
		return cooldownRemaining == 0 ? false : true;
	}

	public void onHit() {
	}
	
	public void postUpkeep() {
		//do nothing
	}
	
	public void setCurStacks(int curStacks) {
		this.stacksCur = curStacks;
	}
	
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	public void setMaxStacks(int maxStacks) {
		this.stacksMax = maxStacks;
	}
	
	public void startCooldown() {
		this.cooldownRemaining = maxCooldown;
	}
}
