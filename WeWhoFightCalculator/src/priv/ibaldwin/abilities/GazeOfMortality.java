package priv.ibaldwin.abilities;

import priv.ibaldwin.PlayerCharacter;
import priv.ibaldwin.vitalityPacket;
import priv.ibaldwin.vitalityPacket.PacketType;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;

public class GazeOfMortality extends Ability{
	public GazeOfMortality(PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank, 0, 30, 0, 0);
		super.addTrait(Trait.Perception);
		super.addTrait(Trait.SpecialAbility);
	}
	
	@Override
	public void doUpkeep() {
		super.doUpkeep();
	}

	@Override
	public void activate() {
		super.startCooldown();
		
		super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.0, -0.03, 0.0, PacketType.Cost));
	}

	@Override
	public AbilityType getAbilityType() {
		return AbilityType.SpecialAbilityActive;
	}

	@Override
	public String getName() {
		return "Gaze of Mortality";
	}
	
	
}
