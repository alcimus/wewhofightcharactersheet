package priv.ibaldwin.abilities;

import priv.ibaldwin.PlayerCharacter;
import priv.ibaldwin.vitalityPacket;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;
import priv.ibaldwin.vitalityPacket.PacketType;

public class UnrelentingAssault extends Ability {
	
	public UnrelentingAssault(PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank);
		super.addTrait(Trait.Conjuration);
		super.addTrait(Trait.Dimension);
		super.addTrait(Trait.SpecialAttack);
		super.setMaxStacks(super.getFullRank());
	}
	
	@Override
	public void activate() {
		super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.0, 0.00, -0.04, PacketType.Cost));
	}

	@Override
	public void disable() {
		super.setCurStacks(0);
	}
	
	@Override
	public void onHit() {
		super.incrementStacks(super.getRankAsInt());
	}

	@Override
	public AbilityType getAbilityType() {
		return AbilityType.SpecialAttack;
	}

	@Override
	public String getName() {
		return "Unrelenting Assault";
	}

	@Override
	public boolean hasOnHitEffects() {
		return true;
	}

}
