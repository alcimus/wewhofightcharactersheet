package priv.ibaldwin.abilities;

import priv.ibaldwin.PlayerCharacter;
import priv.ibaldwin.vitalityPacket;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;
import priv.ibaldwin.vitalityPacket.PacketType;

public class CyclesEmbrace extends Ability{
	
	public CyclesEmbrace(PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank, 0, 60, 0, 0);
		super.addTrait(Trait.SpecialAbility);
		super.addTrait(Trait.Restoration);
	}
	
	@Override
	public void modifyPacket(vitalityPacket packet) {
		if(packet == null)
			return;
		
		int rawMp = packet.getTotalMp(); 
		int rawStam = packet.getTotalStam();
		
		if(rawMp < 0 && rawStam == 0) {
			rawStam = Integer.max(-1 * rawMp / 2, 1);
			super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName() + " from " + packet.getSource(), 0, 0, rawStam, PacketType.Recovery));
		} else if (rawMp == 0 && rawStam < 0) {
			rawMp = Integer.max(-1 * rawStam / 2, 1);
			super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName() + " from " + packet.getSource(), 0, rawMp, 0, PacketType.Recovery));
		}
		
		if(super.getCharacter().getPercentMp() < 25.0) {
			super.setDuration(60);
		}
		
		if(super.getCharacter().getPercentStam() < 25.0) {
			if(!super.onCooldown())
				super.startCooldown();
		}
	}
	
	@Override
	public void disable() {
		super.setMaxStacks(0);
	}
	
	@Override
	public AbilityType getAbilityType() {
		return AbilityType.SpecialAbilityPassive;
	}

	@Override
	public String getName() {
		return "Cycles Embrace";
	}

	@Override
	public boolean isActive() {
		return true;
	}
	
}
