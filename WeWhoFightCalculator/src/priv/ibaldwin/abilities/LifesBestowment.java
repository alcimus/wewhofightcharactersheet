package priv.ibaldwin.abilities;

import priv.ibaldwin.PlayerCharacter;
import priv.ibaldwin.vitalityPacket;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;
import priv.ibaldwin.vitalityPacket.PacketType;

public class LifesBestowment extends Ability{
	
	public LifesBestowment(PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank);
		super.addTrait(Trait.SpecialAbility);
		super.addTrait(Trait.Life);
		super.addTrait(Trait.Restoration);
	}
	
	@Override
	public void doUpkeep() {
		if(super.getCharacter().getPercentStam() == 100.0) {
			super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.01, 0.00, 0.00, PacketType.Recovery));
		}
	}
	
	@Override
	public void modifyPacket(vitalityPacket packet) {
		packet.convertToRaw();
		if(packet.getRawHp() > 0 && packet.isHealing()) {
			packet.addRaw(0, 0, Integer.max((int) Math.floor(packet.getRawHp()/2.0), 1));
		}
	}

	@Override
	public AbilityType getAbilityType() {
		return AbilityType.SpecialAbilityPassive;
	}

	@Override
	public String getName() {
		return "Life's Bestowment";
	}

	@Override
	public boolean isActive() {
		return true;
	}
	
}
