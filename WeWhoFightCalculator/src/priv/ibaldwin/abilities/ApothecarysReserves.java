package priv.ibaldwin.abilities;

import priv.ibaldwin.PlayerCharacter;
import priv.ibaldwin.vitalityPacket;
import priv.ibaldwin.vitalityPacket.PacketType;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;

public class ApothecarysReserves extends Ability{
	
	public ApothecarysReserves(PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank);
		super.addTrait(Trait.Conjuration);
		super.addTrait(Trait.Dimension);
		super.addTrait(Trait.SpecialAbility);
	}
	
	@Override
	public void activate() {
		super.getCharacter().enqueVitalityPacket(new vitalityPacket(this.getName(), 0.0, -0.05, 0.0, PacketType.Cost));
	}

	@Override
	public AbilityType getAbilityType() {
		return Ability.AbilityType.SpecialAbilityActive;
	}

	@Override
	public String getName() {
		return "Apothecary's Reserves";
	}

}
