package priv.ibaldwin.abilities;

import priv.ibaldwin.PlayerCharacter;
import priv.ibaldwin.enums.Rank;
import priv.ibaldwin.enums.Trait;

public class MightyStrength extends Ability{
	public MightyStrength(PlayerCharacter character, Rank rank, int subRank) {
		super(character, rank, subRank);
		super.addTrait(Trait.SpecialAbility);
	}
	
	@Override
	public AbilityType getAbilityType() {
		return AbilityType.SpecialAbilityPassive;
	}

	@Override
	public String getName() {
		return "Mighty Strength";
	}

	@Override
	public boolean isActive() {
		return true;
	}

}
